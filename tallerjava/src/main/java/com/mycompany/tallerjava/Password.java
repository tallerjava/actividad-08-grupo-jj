package com.mycompany.tallerjava;

public class Password {

    private static final int MINIMO = 8;
    private static final int MAXIMO = 20;

    public static void validarPassword(String password) throws PasswordInvalidoException {

        if (password == null || password.length() == 0) {
            throw new IllegalArgumentException("Password Vacío o Referencia en NULL.");
        }

        if (password.length() == 0) {
            throw new IllegalArgumentException("Password Vacío.");
        }

        if (password.length() < MINIMO) {
            throw new PasswordInvalidoException("Cantidad de caracteres menor al mínimo permitido.");
        }

        if (password.length() > MAXIMO) {
            throw new PasswordInvalidoException("Cantidad de caracteres mayor al máximo permitido.");
        }

        
        String buscarnumero = password.replaceAll("[^0-9]", "");
 
        if (buscarnumero.equals("")) {
        throw new PasswordInvalidoException("No hay ningún digito, se requiere al menos uno.");
      }
        
        
        
    }

    public static void main(String[] args) {
        try {
            Password.validarPassword("hosssss1");
            System.out.print("PASS OK");
        } catch (PasswordInvalidoException e) {
            System.out.print(e.getMessage());
        }
    }
}
