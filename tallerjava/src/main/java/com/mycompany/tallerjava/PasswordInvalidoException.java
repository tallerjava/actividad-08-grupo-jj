package com.mycompany.tallerjava;

class PasswordInvalidoException extends Exception {

    public PasswordInvalidoException(String mensaje) {
        super(mensaje);
    }
}
