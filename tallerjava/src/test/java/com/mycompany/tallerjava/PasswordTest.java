package com.mycompany.tallerjava;

import org.junit.Test;

public class PasswordTest {

    @Test(expected = IllegalArgumentException.class)
    public void validarPassword_PasswordVacio_IllegalArgumentException() throws PasswordInvalidoException {
        Password.validarPassword("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void validarPassword_PasswordNull_IllegalArgumentException() throws PasswordInvalidoException {
        Password.validarPassword(null);
    }

    @Test(expected = PasswordInvalidoException.class)
    public void validarPassword_CantidadDeCaracteresMenorQueLoPermitido_PasswordInvalidoException() throws PasswordInvalidoException {
        Password.validarPassword("holaaa1");
    }

    @Test(expected = PasswordInvalidoException.class)
    public void validarPassword_CantidadDeCaracteresMayorQueLoPermitido_PasswordInvalidoException() throws PasswordInvalidoException {
        Password.validarPassword("hola1aaa22222kkkkkkk8");
    }

    @Test
    public void validarPassword_CantidadDeCaracteresIgualAlMinimoPermitidoConCaracterNumerico_void() throws PasswordInvalidoException {
        Password.validarPassword("ddddddd1");
    }

    @Test
    public void validarPassword_CantidadDeCaracteresIgualAlMaximoPermitidoConCaracterNumerico_void() throws PasswordInvalidoException {
        Password.validarPassword("qqqqqqq2qqqqqqqqqqqq");
    }

    @Test(expected = PasswordInvalidoException.class)
    public void validarPassword_SinCaracterNumerico_PasswordInvalidoException() throws PasswordInvalidoException {
        Password.validarPassword("dddddddwr");
    }

    @Test
    public void validarPassword_CantidadDeCaracteresDentroDeLoPermitidoConCaracterNumerico_PasswordInvalidoException() throws PasswordInvalidoException {
        Password.validarPassword("jdke56kdl0");
    }
}
